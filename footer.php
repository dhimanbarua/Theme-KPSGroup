<!--Footer-->
<footer id="footer" class="footer tCenter">



    <!--Footer top-->
    <div class="footerTop ofsInTop ofsInBottom">
        <!--Container-->
        <div class="container clearfix">

            <!--Footer top inner-->
            <div class="ftInner">

                <div class="info one-third column">
                    <span><i class="<?php global $redux_kpsgroup; echo $redux_kpsgroup['first-icon']; ?>"></i><?php echo $redux_kpsgroup["mobile-no"]; ?></span>
                </div>


                <div class="info one-third column">

                    <div class="mail">
                        <i class="<?php echo $redux_kpsgroup["second-icon"]; ?>"></i>
                        <a href=""><?php echo $redux_kpsgroup["email-id"]; ?></a>
                    </div>
                </div>


                <div class="info one-third column">
                    <span><i class="<?php echo $redux_kpsgroup["third-icon"]; ?>"></i><?php echo $redux_kpsgroup["location"]; ?></span>
                </div>


            </div>
            <!--End footer top inner-->

        </div>
        <!--End container-->

    </div>
    <!--End footer top-->




    <!--Footer bottom-->
    <div class="footerBottom  ofsBMedium">
        <!--Container-->
        <div class="container clearfix">

            <!--Footer bottom inner-->
            <div class="fbInner">

                <div class="top">
                    <a class="scroll" href="#top"><i style="<?php echo "color: ". $redux_kpsgroup["backTotop"]; ?>" class=" icon-angle-double-up"></i></a>
                </div>

                <p>&copy; 2017 <span class="brand"><?php echo $redux_kpsgroup["agency-name"]; ?></span> All Rights Reserved.</p>

                <!--Socials teaser-->
                <ul class="socialsFooter">

                    <li><a href="#"><i class="<?php echo $redux_kpsgroup["facebook"]; ?>"></i></a></li>
                    <li><a href="#"><i class="<?php echo $redux_kpsgroup["linkedin"]; ?>"></i></a></li>
                    <li><a href="#"><i class="<?php echo $redux_kpsgroup["twitter"]; ?>"></i></a></li>
                    <li><a href="#"><i class="<?php echo $redux_kpsgroup["instagram"]; ?>"></i></a></li>

                </ul>
                <!--End socials teaser-->

            </div>

            <!--End footer bottom inner-->


        </div>
        <!--End container-->
    </div>
    <!--End footer bottom-->


</footer>
<!--End footer-->










</div>
<!--End wrapper-->


<!--Javascript-->



<!-- Google analytics -->


<!-- End google analytics -->

<?php wp_footer(); ?>
</body>


</html>