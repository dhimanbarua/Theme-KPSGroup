<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>


<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo("charset"); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">



    <!--Stylesheet-->

    <?php wp_head(); ?>
</head <?php body_class(); ?>>

<body>

<!-- Preloader -->
<div id="loader">
    <!-- Preloader inner -->
    <div id="loaderInner">

        <!-- Loader bars
        <div class="loaderBars">
            <span class="bar1 bar"></span>
            <span class="bar2 bar"></span>
            <span class="bar3 bar"></span>
        </div>
        End loader bars -->

    </div>
    <!-- End preloader inner -->
</div>
<!-- End preloader -->



<!--Wrapper-->
<div id="wrapper">




    <!--Header-->
    <header id="header" >

        <!--Main header-->
        <div class="mainHeader fixed">


            <!--Container-->
            <div class="container clearfix">
                <div class="three columns logoHolder">
                    <!--Logo-->
                    <div class="logo">
                        <a  href="<?php home_url(); ?>"><img src="<?php global $redux_kpsgroup; echo $redux_kpsgroup["logo-upload"]["url"]; ?>" alt=""/></a>
                    </div>
                    <!--End logo-->
                </div>


                <div class="thirteen columns tRight">

                    <a href="#" class="mobileBtn" ><i class="icon-menu"></i></a>
                    <!--Navigation-->
                    <nav class="mainNav" >


                        <ul>
                            <li><a href="index-2.html#about">About us</a></li>
                            <li><a href="index-2.html#services">Services</a></li>
                            <li><a href="index-2.html#pricing">Pricing</a></li>
                            <li><a href="index-2.html#portfolio">Portfolio</a></li>
                            <li><a href="index-2.html#blog">Blog</a></li>
                            <li><a href="index-2.html#contact">Contact</a></li>

                        </ul>

                    </nav>
                    <!--End navigation-->

                </div>
                <!--End container-->




            </div>
            <!--End main header-->
        </div>

    </header>
    <!--End header-->