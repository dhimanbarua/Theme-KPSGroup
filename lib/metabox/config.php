<?php

add_action("cmb2_admin_init", "themekps_metabox");
function themekps_metabox(){
    $themekps = new_cmb2_box(array(
        "title"         => __("Format Additional Fields", "kpsgroup"),
        "id"            => ("kps-post-box"),
        "object_types"  => array("post"),
    ));

    $themekps->add_field(array(
        "id"    => "_for-video",
        "type"  => "oembed",
        "name"  => "Video URL"
    ));

    $themekps->add_field(array(
        "id"    => "_for-audio",
        "type"  => "oembed",
        "name"  => "Audio URL"
    ));

    $themekps->add_field(array(
        "id"    => "_for-gallery",
        "type"  => "file_list",
        "name"  => "Gallery Images"
    ));

    $themekps->add_field(array(
        "id"    => "_for-image",
        "type"  => "file",
        "name"  => "Single Image"
    ));

}
add_action("cmb2_admin_init", "themekps_slide_metabox");
function themekps_slide_metabox(){
    $themekps_slide = new_cmb2_box(array(
       "title"          => __("Format Slider Additional Fileds", "kpsgroup"),
        "id"            => ("kps-slide-box"),
        "object_types"   => array("home-slider"),
    ));

    $themekps_slide->add_field(array(
       "id"     => "_slider-logo",
        "name"  => "Slider Logo",
        "type"  => "file",
    ));

    $themekps_slide->add_field(array(
        "id"        => "_for-content",
        "name"      => "Slider Content",
        "type"      => "wysiwyg",
        "options"   => array(
            "wpautop"       => true,
            "textarea_rows" => get_option('default_post_edit_rows', 6),
        )
    ));
}
add_action("cmb2_admin_init", "themekps_social_metabox");
function themekps_social_metabox(){
    $themekps_social = new_cmb2_box(array(
        "title"         => __("Slider Social Icon", "kpsgroup"),
        "id"            => ("kps-slider-social-icon"),
        "object_types"  =>  array("home-slider")
    ));

    $themekps_social->add_field(array(
        "id"        => "_for-facebookurl",
        "name"      => "Facebook URL",
        "type"      => "text_url"
    ));
    $themekps_social->add_field(array(
        "id"        => "_for-facebookicon",
        "name"      => "Facebook Icon",
        "type"      => "text",
    ));
    $themekps_social->add_field(array(
        "id"        => "_for-linkedinurl",
        "name"      => "Linkedin URL",
        "type"      => "text_url"
    ));
    $themekps_social->add_field(array(
        "id"        => "_for-linkedinicon",
        "name"      => "Linkedin Icon",
        "type"      => "text",
    ));
    $themekps_social->add_field(array(
        "id"        => "_for-twitterurl",
        "name"      => "Twitter URL",
        "type"      => "text_url"
    ));
    $themekps_social->add_field(array(
        "id"        => "_for-twittericon",
        "name"      => "Twitter Icon",
        "type"      => "text",
    ));
    $themekps_social->add_field(array(
        "id"        => "_for-instagramurl",
        "name"      => "Instagram URL",
        "type"      => "text_url"
    ));
    $themekps_social->add_field(array(
        "id"        => "_for-instagramicon",
        "name"      => "Instagram Icon",
        "type"      => "text",
    ));


}