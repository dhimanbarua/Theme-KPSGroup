
<?php get_header(); ?>






    <!--Blog single-->
    <section class="blogSingle bgGrey ofsTop  tCenter">

        <!--Container-->
        <div class="container clearfix">



            <!--Blog single intro inner-->
            <div class="pSingleIntroInner singleOffset ofsBottom">

                <!--Title-->
                <?php while(have_posts()): the_post(); ?>
                <div class="title">
                    <h1><?php the_title(); ?><span class="plus">+</span></h1>
                </div>
                <!--End title-->


                <!--Post meta-->
                <div class="postMeta ofsTSmall ofsBSmall">
                    <span class="metaCategory"><a href="#"><?php the_tags(); ?> - </a></span>
                    <span class="metaDate"><a href="#"><?php the_time("d M"); ?> - </a></span>
                    <span class="metaComments"><a href="#"><?php comments_number(); ?></a></span>
                </div>
                <!--End post meta-->

                <!--Project intro-->
                <div class="pSingleIntro  ">
                    <p><?php the_content(); ?></p>
                </div>
                <!--End project intro-->




                <!--Projects share-->
                <ul class="pSingleSocials margTMedium">

                    <li><a href="#"><i class="icon-facebook"></i></a></li>
                    <li><a href="#"><i class="icon-linkedin"></i></a></li>
                    <li><a href="#"><i class="icon-twitter"></i></a></li>
                    <li><a href="#"><i class="icon-instagram"></i></a></li>

                </ul>
                <!--End project share-->
                <?php endwhile; ?>

            </div>
            <!--End blog single intro inner-->


        </div>
        <!--End container-->






        <!--Blog single details-->
        <div class="pSingleDetails  bgLight">


            <!--Post navigation-->
            <div class="postNav ofsTop ofsBottom  bgGreyDark">



                <ul>
                    <?php
                        the_post_navigation(array(
                            "prev_text" => '<i class="icon-left-open-big"></i>',
                            "next_text" => '<i class="icon-right-open-big"></i>',
                            "screen_reader_text"    => " "
                        ));
                    ?>

                </ul>
            </div>
            <!--End post navigation-->


            <!--Container-->
            <div class="container clearfix">

                <div class="eleven columns">

                    <!--Blog single details inner-->
                    <div class="pSingleDetailsInner  tLeft  margMTop">


                        <!--Post author-->
                        <div class="pAuthor tCenter margBottom">

                            <!--Author image-->
                            <div class="authorImg ">
                                <img alt="" src="<?php echo get_template_directory_uri(); ?>/images/teamImages/t3.jpg">
                            </div>
                            <!--End author image-->


                            <!--Author name-->
                            <div class="authorName ofsTSmall">
                                <h3><span>posted by</span> <?php echo get_the_author(); ?></h3>
                            </div>
                            <!--End author name-->

                        </div>
                        <!--End post author-->


                        <!--Post Single-->
                        <div class="postSingle">

                            <!--Post content-->
                            <div class="postContent">

                                <!--Post image-->
                                <div class="postMedia video">
                                   <?php
                                        $audio = get_post_meta(get_the_ID(), "_for-audio", true);
                                        $video = get_post_meta(get_the_ID(), "_for-video", true);

                                        if(!empty($audio)){
                                           echo wp_oembed_get($audio);
                                        }elseif(!empty($video)) {
                                            echo wp_oembed_get($video);
                                        }else {
                                            the_post_thumbnail();
                                        }
                                   ?>

                                </div>
                                <!--End post image-->

                                <p><?php the_content(); ?></p>



                                <div class="tagsSingle clearfix">
                                    <h4><i class="icon-tag-1"></i>Tags :</h4>
                                    <?php
                                    if(get_the_tag_list()){
                                        echo get_the_tag_list('<ul class="tagsListSingle"><li>', '</li> <li>', '</li><ul>');
                                    }
                                    ?>
                                </div>



                            </div>
                            <!--End post content-->

                        </div>
                        <!--End post single-->


                    </div>
                    <!--End blog single details inner-->


                </div>



                <?php get_sidebar(); ?>






            </div>
            <!--End container-->


        </div>
        <!--End blog single details-->


        <?php comments_template(); ?>





    </section>
    <!--End blog single-->



    <?php get_footer(); ?>