<?php
    get_header();
    global $redux_kpsgroup;
?>



    <!--Header single-->
    <section class="headerSingle headerBlog overlay hSingleHeight tCenter">
        <div class="headerBlog">
          <img src="<?php echo $redux_kpsgroup["bg-image"]["url"]; ?>" alt="">
        </div>
        <!--Hero-->
        <div class="hero">


            <!--Title-->
            <div class="title light ofsBottom">
                <h1><?php echo $redux_kpsgroup["blog-title"]; ?><span class="plus">+</span></h1>
            </div>
            <!--End title-->
        </div>
        <!--End hero-->


    </section>
    <!--End header single-->





    <!--Blog large-->
    <section class="blogLarge tCenter bgWhite">



        <!--BLog posts-->
        <div class="blogPosts ">


            <!--Post navigation-->
            <div class="postNav ofsTop ofsBottom  bgGreyDark">


                <?php
                    if(function_exists("the_posts_pagination"))
                    {
                     the_posts_pagination(array(
                        "type"                  => "list",
                         "screen_reader_text"   => " ",
                         "prev_text"            => '<i class="icon-left-open-big"></i>',
                         "next_text"            => '<i class="icon-right-open-big"></i>'
                     ));
                    }
                ?>






            </div>
            <!--End post navigation-->



            <!--Post holder-->
            <div class="postsHolder clearfix tLeft margTop ofsInBottom">



                <!--Container-->
                <div class="container clearfix">




                    <!--Post large holder-->
                    <div class="postLargeHolder">

                       <?php if(have_posts()): ?>
                          <?php while(have_posts()): the_post(); ?>
                               <?php get_template_part("formats/content", get_post_format()); ?>
                           <?php endwhile; ?>

                           <?php else: ?>
                           <h2>No Post Found</h2>
                       <?php endif; ?>




                    </div>
                    <!--Post large holder-->



                </div>
                <!--End container-->


            </div>
            <!--End post holder-->



        </div>
        <!--End blog posts-->


    </section>
    <!--End blog large-->

<?php get_footer(); ?>