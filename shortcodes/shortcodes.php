<?php
add_shortcode("home-slider", "kps_home_slider");
    function kps_home_slider(){

        ob_start(); ?>
        <!--Main slider holder-->
        <div class="mainSliderHolder">

            <!--Main slider-->
            <div class="mainSlider flexslider">


                <!--Slides-->
                <ul class="slides">
                    <?php
                        $sliders = new WP_Query(array(
                            "post_type"         => "home-slider",
                            "posts_per_page"    => 2
                        ));
                    while($sliders->have_posts()): $sliders->the_post();
                    ?>
                    <li class="overlay"><?php the_post_thumbnail('', array('class' => 'slide')); ?></li>
                    <?php endwhile; ?>
                </ul>
                <!--End slides-->


                <!--Slides inner-->
                <div class="slidesInner">
                    <img src="<?php echo get_post_meta(get_the_ID(), '_slider-logo', true); ?>" alt=""/>
                    <p><?php echo get_post_meta(get_the_ID(), '_for-content', true); ?></p>



                    <!--Socials slider-->
                    <ul class="socialsSlider">

                        <li><a href="<?php echo get_post_meta(get_the_ID(), '_for-facebookurl', true); ?>"><i class="<?php echo get_post_meta(get_the_ID(), '_for-facebookicon', true); ?>"></i></a></li>
                        <li><a href="<?php echo get_post_meta(get_the_ID(), '_for-linkedinurl', true); ?>"><i class="<?php echo get_post_meta(get_the_ID(), '_for-linkedinicon', true); ?>"></i></a></li>
                        <li><a href="<?php echo get_post_meta(get_the_ID(), '_for-twitterurl', true); ?>"><i class="<?php echo get_post_meta(get_the_ID(), '_for-twittericon', true); ?>"></i></a></li>
                        <li><a href="<?php echo get_post_meta(get_the_ID(), '_for-instagramurl', true); ?>"><i class="<?php echo get_post_meta(get_the_ID(), '_for-instagramicon', true); ?>"></i></a></li>

                    </ul>
                    <!--End socials slider-->

                </div>

                <!--End slides inner-->


            </div>
            <!--End main slider-->
        </div>
        <!--End main slider holder-->
    <?php return ob_get_clean();
    }
add_shortcode("about-section", "about_section");
function about_section($attr, $content=Null){
    $attributes = extract(shortcode_atts(array(
        'title'     => 'Welcome to our Agency <span class="plus">+</span>',
        'subtitle'  => 'We Design and Develop for the <span>Web</span> &amp; <span>Mobile</span>',
    ), $attr));
    ob_start(); ?>
    <section id="about" class="tCenter">
    <div class="aboutIntro bgGreyDark  ofsTop ofsBottom">


        <!--Container-->
        <div class="container clearfix">

            <!--Title-->
            <div class="title light ofsBMedium">
                <h1><?php echo $title; ?></h1>
            </div>
            <!--End title-->


            <!--About intro content-->
            <div class="aboutIntroContent">
                <h1><?php echo $subtitle; ?></h1>
                <p><?php echo do_shortcode($content); ?></p>

                <a class="btn" href="#">view our portfolio</a>
            </div>
            <!--End about intro content-->

        </div>
        <!--End container-->

    </div>


        </section>
    <!--End about intro-->
    <?php return ob_get_clean();
}
add_shortcode("service-section", "service_section");
function service_section($attr, $content=Null){
    $attributes = extract(shortcode_atts(array(
        'title'         =>  'what we do<span class="plus">+</span>',
        'first_icon'    => 'feather',
        'first_title'   => 'Branding &amp; print',
        'first_content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.','first_icon'    => 'feather',
        'second_icon'    => 'tools',
        'second_title'   => 'design &amp; development',
        'second_content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.','second_icon'    => 'tools',
        'third_icon'    => 'tools',
        'third_title'   => 'design &amp; development',
        'third_content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.',
    ), $attr));
    ob_start(); ?>
    <section id="about" class="tCenter">
        <div class="expertiseHolder margLBottom">


            <!--Container-->
            <div class="container clearfix">


                <!--Title-->
                <div class="title dark ofsBottom">
                    <h1><?php echo $title; ?></h1>
                </div>
                <!--End title-->


                <!--Expertise-->
                <div class="exp one-third column tCenter ">

                    <!--Expertise ico-->
                    <div class="expIco ">
                        <i class="icon-<?php echo $first_icon; ?>"></i>
                    </div>

                    <!--End expertise ico-->


                    <!--Expertise details-->
                    <div class="expDet">
                        <h2 class="expTitle"><?php echo $first_title; ?> </h2>
                        <p><?php echo $first_content; ?></p>
                    </div>
                    <!--End Expertise details-->
                </div>
                <!--End expertise-->


                <!--Expertise-->
                <div class="exp one-third column tCenter selected ">

                    <!--Expertise ico-->
                    <div class="expIco ">
                        <i class="icon-<?php echo $second_icon; ?>"></i>
                    </div>

                    <!--End expertise ico-->


                    <!--Expertise details-->
                    <div class="expDet">
                        <h2 class="expTitle"><?php echo $second_title; ?> </h2>
                        <p><?php echo $second_content; ?></p>
                    </div>
                    <!--End Expertise details-->
                </div>
                <!--End expertise-->



                <!--Expertise-->
                <div class="exp one-third column tCenter ">

                    <!--Expertise ico-->
                    <div class="expIco ">
                        <i class="icon-<?php echo $third_icon; ?>"></i>
                    </div>

                    <!--End expertise ico-->


                    <!--Expertise details-->
                    <div class="expDet">
                        <h2 class="expTitle"><?php echo $third_title; ?> </h2>
                        <p><?php echo $third_content; ?></p>
                    </div>
                    <!--End Expertise details-->
                </div>
                <!--End expertise-->



            </div>
            <!--End container-->


        </div>


        </section>
    <!--End about intro-->
    <?php return ob_get_clean();
}
// add more service section
add_shortcode('more-service', 'more_service');
function more_service($attr, $content=Null){
    $attributes = extract(shortcode_atts(array(
        'title'                     => 'more services<span class="plus">+</span>',
        'first_accordion_title'     => 'Webmarketing',
        'first_accordion_content'   => 'Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                                        ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                                        amet rutrum commodo, vehicula tempus.',
        'second_accordion_title'    => 'Motion graphics',
        'second_accordion_content'  => 'Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                                        ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                                        amet.',
        'third_accordion_title'     => 'WordPress',
        'third_accordion_content'   => 'Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                                        ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                                        amet.',
        'first_tab_title'           => 'marketing',
        'first_tab_content'         => 'Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. 
                                        Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. 
                                        Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris <span class="highlight-2">dapibus lacus auctor</span> risus.<br><br> <span class="bold">Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat.</span> ',
        'second_tab_title'          => 'seo',
        'second_tab_content'        => 'Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris <span class="highlight-2">dapibus lacus auctor</span> risus.<br><br> <span class="bold">Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat.</span> ',
        'third_tab_title'           => 'socials media',
        'third_tab_content'         => 'Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris <span class="highlight-2">dapibus lacus auctor</span> risus.<br><br> <span class="bold">Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat.</span> '

    ), $attr));
    ob_start() ?>
    <!--More services-->
    <section id="services" class="tCenter">
        <!--More services-->
        <div class="more bgGrey   ofsBottom">
            <!--Title-->
            <div class="title dark ofsInBottom">
                <h1><?php echo $title; ?></h1>
            </div>
            <!--End title-->
            <!--Container-->
            <div class="container clearfix">
                <!--More holder-->
                <div class="moreHolder tLeft clearfix">

                    <div class="eight columns">

                        <!--Accordion content-->
                        <div id="accordionContent">
                            <div id="accordion" class="forceBackground">
                                <h3><a href="#"><?php echo $first_accordion_title; ?></a></h3>
                                <div>
                                    <p>

                                        <?php echo $first_accordion_content; ?>
                                    </p>
                                </div>
                                <h3><a href="#"><?php echo $second_accordion_title; ?></a></h3>
                                <div>
                                    <p>
                                        <?php echo $second_accordion_content; ?>

                                    </p>
                                </div>
                                <h3><a href="#"><?php echo $third_accordion_title; ?></a></h3>
                                <div>
                                    <p>
                                        <?php echo $third_accordion_content;  ?>

                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--End accordion content-->


                    </div>


                    <div class="eight columns tabs">


                        <!--Tabs content-->
                        <div id="tabsContent">
                            <div id="tabs">
                                <ul class="borderForce">
                                    <li><a href="#tabs-1"><?php echo $first_tab_title ?></a></li>
                                    <li><a href="#tabs-2"><?php echo $second_tab_title ?></a></li>
                                    <li class="bNone"><a href="#tabs-3"><?php echo $third_tab_title; ?></a></li>
                                </ul>
                                <div id="tabs-1">
                                    <p><?php echo $first_tab_content; ?></p>
                                </div>
                                <div id="tabs-2">
                                    <p><?php echo $second_tab_content; ?></p>
                                </div>

                                <div id="tabs-3">
                                    <p><?php echo $third_tab_content; ?></p>
                                </div>
                            </div>
                        </div>
                        <!--End tabs content-->


                    </div>


                </div>
                <!--End more holder-->

            </div>
            <!--End container-->


        </div>
        <!--End more services-->
    </section>
    <!--End more services-->
    <?php return ob_get_clean();
}
add_shortcode("event-section", "event_section");
function event_section(){
    ob_start(); ?>
    <section>
        <!--Process-->
        <div class="process overlay tCenter ofsTop ofsBottom ">
            <!--Container-->
            <div class="container clearfix">
                <div class="smallIntro">
                    <p>We would like to share with you our <span>process</span>
                        when working on our projects
                    </p>
                </div>
                <!--Process holder-->
                <div class="processHolder  ofsTop clearfix">
                    <!--Process single-->
                    <div class="prc one-third column ">
                        <div class="prcDet">
                            <div class="prcIco"><i class="icon-droplet"></i>	</div>
                            <h3>Brainstorming</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore.</p>
                        </div>
                    </div>
                    <!--End process single-->
                    <!--Process single-->
                    <div class="prc one-third column ">
                        <div class="prcDet">
                            <div class="prcIco selected">	<i class="icon-tools"></i></div>
                            <h3>Design / Development</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore.</p>
                        </div>
                    </div>
                    <!--End process single-->
                    <!--Process single-->
                    <div class="prc one-third column ">
                        <div class="prcDet">
                            <div class="prcIco">	<i class="icon-basket"></i></div>
                            <h3>Client Delivrey</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore.</p>

                        </div>
                    </div>
                    <div class="prc bottom-column one-third column ">
                        <div class="prcDet">
                            <div class="prcIco">	<i class="icon-basket"></i></div>
                            <h3>Client Delivrey</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore.</p>

                        </div>
                    </div>

                    <!--End process single-->
                </div>
                <!--End process holder-->

            </div>
            <!--End container-->
        </div>

        <!--End process-->
    </section>
    <?php return ob_get_clean();
}
