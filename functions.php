<?php
/**
* KPS-Group functions and definitions
* Anybody can use the theme but he/she will have to maintain the GPL 2 licence
* Here you will get all the functions of KPS-Group
 *
 **/

// CMB2
if(file_exists(dirname(__FILE__)."/lib/metabox/init.php")){
    require_once(dirname(__FILE__)."/lib/metabox/init.php");
}
if(file_exists(dirname(__FILE__)."/lib/metabox/config.php")){
    require_once(dirname(__FILE__)."/lib/metabox/config.php");
}
// Redux FramWork
if(file_exists(dirname(__FILE__)."/lib/ReduxCore/framework.php")){
    require_once(dirname(__FILE__)."/lib/ReduxCore/framework.php");
}
if(file_exists(dirname(__FILE__)."/lib/sample/custom-config.php")){
    require_once(dirname(__FILE__)."/lib/sample/custom-config.php");
}
// Widgets
if(file_exists(dirname(__FILE__)."/widgets.php")){
    require_once(dirname(__FILE__)."/widgets.php");
}
if(file_exists(dirname(__FILE__)."/shortcodes/shortcodes.php")){
    require_once(dirname(__FILE__)."/shortcodes/shortcodes.php");
}


add_action("after_setup_theme", "kpsgroup_functions");
function kpsgroup_functions(){
    // text domain
    load_theme_textdomain("kpsgroup", get_template_directory()."/lang");

    // theme supports
    add_theme_support("title-tag");
    add_theme_support("post-thumbnails");
    add_theme_support("post-formats", array(
        "video",
        "audio",
        "standard",
        "quote",
        "image",
        "gallery"
    ));
    // main menu
}
//right sidebar
add_action("widgets_init", "kps_sidebar");
function kps_sidebar(){
    register_sidebar(array(
        "name"          => __("Right Sidebar", "kpsgroup"),
        "description"   => __("You may add your right Sidebar Widgets here", "kpsgroup"),
        "id"            => "right-sidebar",
        "before_widget" => '<div class="widget">',
        "after_widget"  => '</div>',
        "before_title"  => '<h2>',
        "after_title"   => '</h2>',
    ));
}
// KPS Slider
register_post_type("home-slider", array(
    "labels"            => array(
        "name"          => __("Slider", "kpsgroup"),
        "Add New"       => __("Add New Slider", "kpsgroup"),
        "Add New Item"  => __("Add New Slider", "kpsgroup")
    ),
    "public"            => true,
    "supports"          => array('thumbnail'),
    "show_in_menu"      => "front-sections"
));
// Front Section
add_action("admin_menu", "front_admin_menu");
function front_admin_menu(){
    add_menu_page(
        "Front Section",
        "Front Section",
        "read",
        "front-sections",
        "",
        "dashicons-admin-home",
        2
    );
}
// adding theme styles
add_action("wp_enqueue_scripts", "theme_styles");
function theme_styles(){
    wp_enqueue_style("font", get_template_directory_uri()."/css/font.css");
    wp_enqueue_style("fontello", get_template_directory_uri()."/css/fontello.css");
    wp_enqueue_style("skeleton", get_template_directory_uri()."/css/skeleton.css");
    wp_enqueue_style("main", get_template_directory_uri()."/css/main.css");
    wp_enqueue_style("magnific-popup", get_template_directory_uri()."/css/magnific-popup.css");
    wp_enqueue_style("flexslider", get_template_directory_uri()."/css/flexslider.css");
    wp_enqueue_style("base", get_template_directory_uri()."/css/base.css");
    wp_enqueue_style("jquery-custom", get_template_directory_uri()."/css/jquery-ui-1.8.23.custom.css");
    wp_enqueue_style("stylesheet", get_stylesheet_uri());
}
// conditional theme style
add_action("wp_enqueue_scripts", "conditional_style");
function conditional_style(){
    wp_enqueue_style("haslayout", get_template_directory_uri()."/css/haslayout.css");
    wp_style_add_data("haslayout", "conditional", "lt IE 8");
}
// conditional theme script
add_action("wp_enqueue_scripts", "conditional_script");
function conditional_script(){
    wp_enqueue_script("html5shiv", "http://html5shiv.googlecode.com/svn/trunk/html5.js", array(), "", false);
    wp_script_add_data("html5shiv", "conditional", "lt IE 9");
}
// adding theme scripts
add_action("wp_enqueue_scripts", "theme_scripts");
function theme_scripts(){
    wp_enqueue_script("jquery-migrate", get_template_directory_uri()."/js/jquery-migrate-1.2.1.js", array("jquery"), "", true);
    wp_enqueue_script("flexslider", get_template_directory_uri()."/js/jquery.flexslider-min.js", array("jquery"), "", true);
    wp_enqueue_script("easing", get_template_directory_uri()."/js/jquery.easing.1.3.js", array("jquery"), "", true);
    wp_enqueue_script("smooth-scroll", get_template_directory_uri()."/js/jquery.smooth-scroll.js", array("jquery"), "", true);
    wp_enqueue_script("quicksand", get_template_directory_uri()."/js/jquery.quicksand.js", array("jquery"), "", true);
    wp_enqueue_script("modernizr", get_template_directory_uri()."/js/modernizr.custom.js", array("jquery"), "", true);
    wp_enqueue_script("magnific", get_template_directory_uri()."/js/jquery.magnific-popup.js", array("jquery"), "", true);
    wp_enqueue_script("tweet", get_template_directory_uri()."/js/jquery.tweet.js", array("jquery"), "", true);
    wp_enqueue_script("spectragram", get_template_directory_uri()."/js/spectragram.min.js", array("jquery"), "", true);
    wp_enqueue_script("parallax", get_template_directory_uri()."/js/jquery.parallax-1.1.3.js", array("jquery"), "", true);
    wp_enqueue_script("timer", get_template_directory_uri()."/js/jquery.timer.js", array("jquery"), "", true);
    wp_enqueue_script("appear", get_template_directory_uri()."/js/jquery.appear.min.js", array("jquery"), "", true);
    wp_enqueue_script("Placeholders", get_template_directory_uri()."/js/Placeholders.min.js", array("jquery"), "", true);
    wp_enqueue_script("jquery-ui", get_template_directory_uri()."/js/jquery-ui-1.8.23.custom.min.js", array("jquery"), "", true);
    wp_enqueue_script("script", get_template_directory_uri()."/js/script.js", array("jquery"), "", true);
}
// Post Formats
add_action("admin_print_scripts", "themekps_sp_script", 1000);
function themekps_sp_script(){ ?>
    <?php if(get_post_type() == "post") : ?>
    <script>
        jQuery(document).ready(function(){

            var id = jQuery('input[name="post_format"]:checked').attr("id");

            if(id == "post-format-video"){
                jQuery(".cmb2-id--for-video").show();
            }else{
                jQuery(".cmb2-id--for-video").hide();
            }

            if(id == "post-format-audio"){
                jQuery(".cmb2-id--for-audio").show();
            }else {
                jQuery(".cmb2-id--for-audio").hide();
            }
            if(id == "post-format-gallery"){
                jQuery(".cmb2-id--for-gallery").show();
            }else {
                jQuery(".cmb2-id--for-gallery").hide();
            }
            if(id == "post-format-image"){
                jQuery(".cmb2-id--for-image").show();
            }else {
                jQuery(".cmb2-id--for-image").hide();
            }

            jQuery('input[name="post_format"]').change(function(){
                jQuery(".cmb2-id--for-video").hide();
                jQuery(".cmb2-id--for-audio").hide();
                jQuery(".cmb2-id--for-gallery").hide();
                jQuery(".cmb2-id--for-image").hide();

                var id = jQuery('input[name="post_format"]:checked').attr("id");


                if(id == "post-format-video"){
                    jQuery(".cmb2-id--for-video").show();
                }else{
                    jQuery(".cmb2-id--for-video").hide();
                }
                if(id == "post-format-audio"){
                    jQuery(".cmb2-id--for-audio").show();
                }else {
                    jQuery(".cmb2-id--for-audio").hide();
                }
                if(id == "post-format-gallery"){
                    jQuery(".cmb2-id--for-gallery").show();
                }else {
                    jQuery(".cmb2-id--for-gallery").hide();
                }
                if(id == "post-format-image"){

                    jQuery(".cmb2-id--for-image").show();
                }else {
                    jQuery(".cmb2-id--for-image").hide();
                }
            })


        })
    </script>
    <?php endif; ?>
<?php }

//login page custom logo
add_action("login_enqueue_scripts", "my_login_logo");
function my_login_logo(){ ?>
    <style type="text/css">
        #login h1 a, .login h1 a{
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logoD.png);
            background-size: 200px 150px;
            width: 300px;
            height: 145px;
            line-height: 1.3em;
        }
    </style>
<?php }
//login page custom logo title
add_filter("login_headertitle", "my_login_logo_title");
function my_login_logo_title(){
    return "Powerd by KPS-Group";
}
//login page custom logo URL
add_filter("login_headerurl", "my_login_logo_url");
function my_login_logo_url(){
    return "http://www.kps-group.biz";
}
// comment form
add_filter("comment_form_default_fields", "kps_comment_form");
function kps_comment_form($deafult){
    $deafult["author"] = '<div class="container replyForm inputColumns clearfix"><div class="column1">
                              <div class="columnInner">
                                 <input type="text" placeholder="Name *" value="" id="name" name="author" >
                              </div>
                             </div>';
    $deafult["email"] = '<div class="column2"><div class="columnInner">
                            <input type="text" placeholder="Email *" value="" id="email" name="email">
                            </div></div></div>';
    $deafult["url"] = '<div class="container replyForm upper"><input type="text" placeholder="WebSite" value="" id="website" name="url" ></div>';
    $deafult["comments-kps"] = '<div class="container replyForm txt-upper"><textarea name="comment"  placeholder="Message *" id="message" cols="45" rows="10"></textarea></div>';



    return $deafult;
}
add_filter("comment_form_defaults", "kps_comment_form_default");
function kps_comment_form_default($default_info){

    if(!is_user_logged_in()){
        $default_info["comment_field"] = '';
    }else{
        $default_info["comment_field"] = '<div class="container replyForm txt-upper"><textarea name="comment"  placeholder="Message *" id="message" cols="45" rows="10"></textarea></div>';
    }

    $default_info["submit_button"] = '<button type="submit" id="submit" class="btn">Post Comment</button>';
    $default_info["submit_field"] = '<div class="container replyForm tLeft  btn-upper">%1$s %2$s</div>';
    $default_info["comment_notes_before"] = '';
    $default_info["title_reply"] = 'Leave a comment';
    $default_info["title_reply_before"] = '<div class="container respond"><h2>';
    $default_info["title_reply_after"] = '</h2></div> ';
    return $default_info;
}

function my_callback_function($comment, $arg, $depth){

        $GLOBALS['comment'] = $comment;
    ?>
    <li>
        <div class="comment">

            <div class="authorImg">
                <?php echo get_avatar($comment); ?>

            </div>
            <div class="commentContent">
                <div class="commentsInfo">
                    <div class="author"><a href="#"><?php echo get_comment_author_link(); ?></a></div>
                    <div class="date"><a href="#"><?php comment_date('F d, Y'); ?> at <?php comment_time('g:i, a'); ?></a></div>
                </div>
                <p class="expert"><?php comment_text(); ?></p>
            </div>

            <div class="reply-btn">
                <?php comment_reply_link(
                    array_merge($arg, array(
                        'depth'     => $depth,
                        'max_dephp' => $arg['max_depth']
                    ))
                ); ?>
            </div>
        </div>
    </li>
<?php }













