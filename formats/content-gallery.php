<div class="postLarge one-third column">

    <!--Post content-->
    <div class="postContent">

        <div class="postTitle">
            <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

            <!--Post meta-->
            <div class="postMeta">
                <span class="metaCategory"><?php the_tags(); ?>-</span>
                <span class="metaDate"><a href="#"><?php the_date("d M"); ?> - </a></span>
                <span class="metaComments"><a href="#"><?php comments_number(); ?></a></span>
            </div>
            <!--End post meta-->

        </div>

        <!--Post image-->
        <div class="postMedia postSlider slider flexslider">
            <ul class="slides">
                <?php
                    $images = get_post_meta(get_the_ID(), '_for-gallery', true);
                ?>
                <?php foreach($images as $image): ?>
                    <li><a href="<?php the_permalink(); ?>"><img src="<?php echo $image; ?>" alt=""/></a></li>
                <?php endforeach; ?>

            </ul>
        </div>
        <!--End post image-->

        <p><?php the_content(); ?></p>


        <a class="btn more border" href="<?php the_permalink(); ?>">Read more</a>

    </div>
    <!--End post content-->

</div>