<div class="postLarge one-third column">

    <!--Post content-->
    <div class="postContent">

        <div class="postTitle">
            <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

            <!--Post meta-->
            <div class="postMeta">
                <span class="metaCategory"><a href="#"><?php the_tags(); ?> - </a></span>
                <span class="metaDate"><a href="#"><?php the_date("d M"); ?> - </a></span>
                <span class="metaComments"><a href="#"><?php comments_number(); ?></a></span>
            </div>
            <!--End post meta-->

        </div>

        <!--Post image-->
        <div class="postMedia">
            <div class="postMedia">
                <?php
                   the_post_thumbnail();
                ?>

            </div>

            
        </div>
        <!--End post image-->

        <p><?php the_content(); ?></p>

        <a class="btn more border" href="blog_single_sb.html">Read more</a>


    </div>
    <!--End post content-->

</div>