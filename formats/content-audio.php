<!--Post large-->
<div class="postLarge one-third column  ">

    <!--Post content-->
    <div class="postContent">


        <div class="postTitle">
            <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>


            <!--Post meta-->
            <div class="postMeta">
                <span class="metaCategory"><?php the_tags(); ?> - </span>
                <span class="metaDate"><a href="#"><?php the_time("d M"); ?> - </a></span>
                <span class="metaComments"><a href="#"><?php comments_number(); ?></a></span>
            </div>
            <!--End post meta-->
        </div>

        <!--Post image-->
        <div class="postMedia audio">
            <?php echo wp_oembed_get(get_post_meta(get_the_ID(), "_for-audio", true)); ?>
        </div>
        <!--End post image-->


        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

        <a class="btn more border" href="blog_single_sb.html">Read more</a>


    </div>
    <!--End post content-->

</div>
<!--End post large-->