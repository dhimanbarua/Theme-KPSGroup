<div class="postLarge one-third column">

    <!--Post content-->
    <div class="postContent">

        <div class="postTitle">
            <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

            <!--Post meta-->
            <div class="postMeta">
                <span class="metaCategory"><?php the_tags(); ?> - </span>
                <span class="metaDate"><a href="#"><?php the_time("d M"); ?> - </a></span>
                <span class="metaComments"><a href="#"><?php comments_number(); ?></a></span>
            </div>
            <!--End post meta-->

        </div>

        <!--Post image-->
        <div class="postMedia">
            <?php echo wp_oembed_get(get_post_meta(get_the_ID(), "_for-video", true)); ?>
        </div>
        <!--End post image-->

        <p><?php the_content(); ?></p>

        <a class="btn more border" href="blog_single.html">Read more</a>


    </div>
    <!--End post content-->

</div>