<?php

?>
    <div class="container clearfix">
        <div class="commentsHolderInner  tLeft thirteen middle columns ">
            <div class="comments">
                <h2><?php comments_number(); ?></h2>
                <div class="entriesContainer">
                    <ul class="comments clearfix">
<?php
wp_list_comments(array(
    "callback" => "my_callback_function"
));
?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<?php
comment_form();