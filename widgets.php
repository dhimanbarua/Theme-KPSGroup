<?php

class kps_recent_post extends WP_Widget{

    public function __construct()
    {
        parent::__construct("KPS-latest-post", "KPS Latest Post", array(
            "description"   => "Custom Latest Post Widget by KPS-Group Theme"
        ));
    }
    public function widget($instance, $count)
    { ?>
        <?php echo $instance["before_widget"]; ?>
            <?php echo $instance["before_title"]; ?>Recents Posts<?php
                   echo $instance["after_title"]; ?>
            <?php
                $posts = new WP_Query(array(
                    "post_type"         => "post",
                    "posts_per_page"    => $count['post_count']
                ));
            ?>
            <ul class="catRecents">
                <?php while($posts->have_posts()): $posts->the_post(); ?>
                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?>  <span>&rarr;</span></a></li>
                <?php endwhile; ?>
            </ul>

        <?php echo $instance["after_widget"]; ?>
    <?php }

    public function form($val)
    { ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
            <input type="text" id="<?php echo $this->get_field_id('title'); ?>" class="widefat" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $val['title']; ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post_count'); ?>">Number of posts to show:</label>
            <input class="tiny-text" id="<?php echo $this->get_field_id('post_count'); ?>" name="<?php echo $this->get_field_name('post_count'); ?>" step="1" min="1" value="<?php echo $val['post_count']; ?>" size="3" type="number">
        </p>
    <?php }
}
add_action("widgets_init", "latest_post_widget");
function latest_post_widget(){
    register_widget("kps_recent_post");
}

class kps_categories extends WP_Widget{
    public function __construct()
    {
        parent::__construct("KPS-categories", "KPS Categories", array(
            "description"   => "Custom Categories Post Widget by KPS-Group Theme"
        ));
    }
    public function widget($instance)
    { ?>
        <?php echo $instance["before_widget"]; ?>
            <?php echo $instance["before_title"]; ?>Categoriess<?php echo $instance["after_title"]; ?>
            <ul class="catList">
                 <?php
                    $categories = new WP_Query(array(
                        "post_type"     => "post"
                    ))
                 ?>
                <?php while($categories->have_posts()): $categories->the_post(); ?>
                <li><a href="#"><?php the_category(); ?>
                        <span></span></a></li>
                <?php endwhile; ?>
            </ul>

        <?php echo $instance["after_widget"]; ?>
    <?php }

    public function form($val)
    { ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
            <input type="text" id="<?php echo $this->get_field_id('title'); ?>" class="widefat" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $val['title']; ?>">
        </p>
    <?php }

}
add_action("widgets_init", "categories_widget");
function categories_widget(){
    register_widget("kps_categories");
}